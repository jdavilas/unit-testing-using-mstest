﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using src.api;


namespace test.api.UnitTests
{
    [TestClass]
    public class MoneyTest
    {
        [TestMethod]
        public void TestDollarMultiplication()
        {

            Dollar five = new Dollar(5);

            Assert.IsTrue(new Dollar(10).Equals(five.Times(2)));
            Assert.IsTrue(new Dollar(15).Equals(five.Times(3)));
           
        }


        [TestMethod]
        public void TestFrancMultiplication()
        {

            Franc five = new Franc(5);
            Assert.ReferenceEquals(new Franc(10), five.Times(2));
            Assert.ReferenceEquals(new Franc(15), five.Times(3));

        }

        [TestMethod]
        [DataRow(5, 6)]
        public void testEquivalentDollar(int x, int y){
            var theBoolean = new Dollar(5);
            theBoolean.Should().BeEquivalentTo(new Dollar(x));
            theBoolean.Should().NotBeEquivalentTo(new Dollar(y));

        }
        [TestMethod]
        [DataRow(5, 6)]
        public void testEqualityDollar(int x, int y)
        {
            Assert.IsFalse(new Dollar(x).Equals(new Dollar(y)));
            Assert.IsTrue(new Dollar(x).Equals(new Dollar(x)));
        }

        [TestMethod]
        [DataRow(5, 6)]
        public void testEqualityFranc(int x, int y)
        {
            var theBoolean = new Dollar(5);
            theBoolean.Should().BeEquivalentTo(new Dollar(x));
            theBoolean.Should().NotBeEquivalentTo(new Dollar(y));
        }

        [TestMethod]
        public void testDollarIsEqualToFranc()
        {
            var dollar = new Dollar(5);
            Assert.IsFalse(dollar.Equals(new Franc(5)));
        }

        [TestMethod]
        public void testFrancIsEqualToDollar()
        {
            var dollar = new Franc(5);
            Assert.IsFalse(dollar.Equals(new Dollar(5)));
        }
    }
}
