﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace api.UnitTests
{
    [TestClass]
    public class MoneyTest
    {
        [TestMethod]
        public void TestMultiplication()
        {

            Dollar five = new Dollar(5);
            Dollar product = five.times(2);
            product.Amount.Should().Be(10);
            product = five.times(3);
            product.Amount.Should().Be(15);
        }
    }
}