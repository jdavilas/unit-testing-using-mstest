﻿using System;
namespace api
{
    public class Money
    {
        public int Amount { get; set; }

        public Money(int amount) => this.Amount = amount;

        public Money Times(int multiplier)
        {
            return new Money(Amount * multiplier);
        }

        public new bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {

                return false;
            }

            Money money = (Money)obj;
            return Amount == money.Amount;

        }


        //public bool Equals(object obj) => this == (Money)obj;
    }
}
